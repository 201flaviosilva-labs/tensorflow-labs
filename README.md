# TensorFlow.js

## Description
Projetos de Teste e aprendizagem em TensorFlow

## Links
- [Web Site](https://master.d12lug54xwnhi4.amplifyapp.com);
- [Code](https://bitbucket.org/201flaviosilva/tensorflow-labs/);
- [TensorFlow.js Official Site](https://www.tensorflow.org/js/);
- [ml5.js Official Site](https://ml5js.org/);
- [P5js Official Site](https://p5js.org/);
