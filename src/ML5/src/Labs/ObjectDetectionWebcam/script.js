let video;
let detector;
let results;

function preload() {
	detector = ml5.objectDetector("cocossd");
	video = createCapture(VIDEO);
	video.hide();
}

function setup() {
	createCanvas(windowWidth, windowHeight);
	// createCanvas(640, 480);
	video.size(width, height);

	text("Loading...", width / 2, height / 2);

	detector.detect(video, detectorResult);
}

// Get the video detector
function detectorResult(error, r) {
	if (error) console.log(error);
	results = r;

	detector.detect(video, detectorResult);
}

function draw() {
	clear();

	image(video, 0, 0, width, height);


	if (results) {
		for (let i = 0; i < results.length; i++) {
			const result = results[i];

			const scaleX = width / 640;
			const scaleY = height / 480;

			const x = result.x * scaleX;
			const y = result.y * scaleY;

			const w = result.width * scaleX;
			const h = result.height * scaleY;

			stroke(0, 255, 0);
			strokeWeight(2);
			noFill();
			rect(x, y, w, h, 6);

			stroke(255, 0, 0);
			fill(255);
			text(`${result.label} (${(result.confidence * 100).toFixed(2)}%)`, x + 5, y + 20);
		}
	}
}
