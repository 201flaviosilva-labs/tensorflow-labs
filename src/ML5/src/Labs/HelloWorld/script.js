let mobilenet;
let ninicaImg;

function preload() {
	ninicaImg = loadImage("assets/Ninica1.jpg");
}

function setup() {
	createCanvas(800, 600);

	image(ninicaImg, 100, 100, 250, 300);

	// ML5
	mobilenet = ml5.imageClassifier("mobilenet", () => {
		mobilenet.predict(ninicaImg, (error, result) => {

			textSize(24);
			text(`${result[0].label} (${result[0].confidence.toFixed(5)})`, 100, 420);
			console.log("Error ->", error);
			console.log("Result ->", result);
		});
	});
}
