// https://www.youtube.com/watch?v=eeO-rWYFuG0&list=PLRqwX-V7Uu6YPSwT06y_AEYTqIwbeam3y&index=7

let mobilenet;
let result;
let classifier;
let label = "";

// UI
let video;
let happyBtn;
let sadBtn;
let trainBtn;
let saveBtn;
let buttonsList;

class Buttons {
	constructor(text = "", x = 0, y = 0, width = 100, height = 50, textSize = 20, textColor = "#ff0000", bgColor = "#000fff", handle = () => { print("click") }) {
		this.text = text;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.textSize = textSize;
		this.textColor = textColor;
		this.bgColor = bgColor;
		this.handle = handle;
	}

	draw() {
		// Rect
		noFill();
		strokeWeight(2);
		stroke(this.bgColor);
		rect(this.x, this.y, this.width, this.height, 4);

		// Text
		noStroke();
		fill(this.textColor);
		textAlign(LEFT);
		textSize(this.textSize);
		text(this.text, this.x + this.textSize, this.y + this.textSize);
	}

	click() {
		this.handle();
	}
}

function setup() {
	createCanvas(windowWidth, windowHeight);

	video = createCapture(VIDEO);
	video.hide();

	// ML5
	mobilenet = ml5.featureExtractor("MobileNet", () => { print("MobileNet loaded") });
	classifier = mobilenet.classification(video, () => { print("Video loaded") });

	{// Btns
		const y = height - 50;
		happyBtn = new Buttons("Feliz", 0, y, 100, 30, 25, "#00ff00", "#000fff", () => classifier.addImage("Feliz"));
		sadBtn = new Buttons("Triste", 120, y, 100, 30, 25, "#00ffff", "#000fff", () => classifier.addImage("Triste"));
		trainBtn = new Buttons("Começar", 240, y, 150, 30, 25, "#ffff00", "#000fff", () => classifier.train(whileTraining));
		saveBtn = new Buttons("Salvar", width - 200, y, 125, 30, 25, "#ff00ff", "#00ffff", () => classifier.save());

		buttonsList = [happyBtn, sadBtn, trainBtn, saveBtn];
	}
}

function whileTraining(loss) {
	if (loss == null) {
		console.log("Training Complete");
		classifier.classify(gotResults);
	} else {
		console.log(loss);
	}
}

function gotResults(error, r) {
	if (error) {
		console.error(error);
	} else {
		result = r[0];
		classifier.classify(gotResults);
	}
}

function draw() {
	clear();

	image(video, 0, 0, width, height);

	{ // Normal Labels
		textSize(48);
		textAlign(CENTER);
		stroke(255, 0, 0);
		strokeWeight(5);
		fill(255);
		text("Classificação por Webcam", width / 2, 50);

		if (result) {
			textSize(24);
			text(result.label + " " + result.confidence, width / 2, height - 50);
		}
	}

	buttonsList.map(btn => btn.draw());
}

function mouseClicked() {
	buttonsList.map(btn => {
		if (collidePointRect(mouseX, mouseY, btn.x, btn.y, btn.width, btn.height)) {
			btn.click();
			print(btn.text);
		}
	});
}
