let videoClassifier;
let mobilenet;
let result;

function preload() { }

function setup() {
	createCanvas(windowWidth, windowHeight);
	fill(255);
	stroke(255, 0, 0);
	strokeWeight(5);
	textAlign(CENTER);

	videoClassifier = createCapture(VIDEO);
	videoClassifier.hide();

	// ML5
	mobilenet = ml5.imageClassifier("mobilenet", videoClassifier, predictClassifier);

}

// Get the video classifier
function predictClassifier() {
	mobilenet.predict((error, r) => {
		if (error) console.log("Error ->", error);

		console.log("Result ->", r);
		result = r;

		predictClassifier();
	});
}

function draw() {
	clear();
	image(videoClassifier, 0, 0, width, height);

	textSize(48);
	text("Classificação por Webcam", width / 2, 50);

	if (result) {
		text(`${result[0].label} (${(result[0].confidence * 100).toFixed(5)})`, width / 2, height - 100);

		let str = "";
		for (let i = 1; i < result.length; i++) {
			str += result[i].label + ", ";
		}

		textSize(24);
		text(str, width / 2, height - 50);
	}
}
