let ninicaImg;
let detector;

const randomNumber = (min = 0, max = 100) => Math.floor(Math.random() * (max - min + 1) + min);

function preload() {
	const stringImg = "assets/Ninica" + randomNumber(1, 9) + ".jpg";
	console.log(stringImg);
	ninicaImg = loadImage(stringImg);
	detector = ml5.objectDetector("cocossd");
}

function setup() {
	createCanvas(800, 600);

	image(ninicaImg, 0, 0);

	detector.detect(ninicaImg, (error, results) => {
		if (error) console.log(error);
		console.log(results);

		for (let i = 0; i < results.length; i++) {
			const result = results[i];
			console.log(result);

			stroke(0, 255, 0);
			strokeWeight(2);
			noFill();
			rect(result.x, result.y, result.width, result.height, 6);

			stroke(255, 0, 0);
			fill(255);
			text(`${result.label} (${(result.confidence * 100).toFixed(2)}%)`, result.x + 5, result.y + 20);
		}
	});
}
